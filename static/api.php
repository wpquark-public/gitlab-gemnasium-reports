<?php

define('ABSPATH', __DIR__);

class API {
	const ABSPATH = __DIR__;
	const SCANDIRECTORY = __DIR__ . '/reports';

	public function stream_response($method = 'branches') {
		if ('branches' === $method) {
			$this->stream_branches();
		} else {
			$this->stream_branch($method);
		}
	}

	protected function stream_branch($branch) {
		// Sanitize against attacks
		$branch = str_replace(['.', '/'], '', $branch);
		$branch_dir = self::SCANDIRECTORY . '/' . $branch;
		$branch_report_file = $branch_dir . '/gl-dependency-scanning-report.json';
		$branch_data = [
			'dependency' => [
				[
					'priority' => 'Unknown',
					'message' => 'No Reports found for this branch',
					'url' => '#',
					'cve' => 'noop',
					'tool' => 'System',
					'file' => $branch,
				],
			],
			'coverage' => [
				'phpunit' => '',
				'jest' => '',
			],
		];
		if (file_exists($branch_report_file)) {
			$branch_data['dependency'] = json_decode(file_get_contents($branch_report_file));
		}

		// Now check for coverage reports
		// 1. PHPUnit Coverage Report
		$phpunit_coverage_file = $branch_dir . '/phpunit/html/index.html';
		if (file_exists($phpunit_coverage_file)) {
			$branch_data['coverage']['phpunit'] = 'reports/' . $branch . '/phpunit/html/index.html';
		}
		// 2. Jest Coverage Report
		$jest_coverage_file = $branch_dir . '/jest/lcov-report/index.html';
		if (file_exists($jest_coverage_file)) {
			$branch_data['coverage']['jest'] = 'reports/' . $branch . '/jest/lcov-report/index.html';
		}
		$this->send_response($branch_data);
	}

	protected function stream_branches() {
		if (!is_dir(self::SCANDIRECTORY)) {
			$this->send_response([]);
			return;
		}
		$scan_report = scandir(self::SCANDIRECTORY);
		$branches = [];
		foreach ($scan_report as $directory) {
			if (in_array($directory, ['.', '..']) || ! is_dir(self::SCANDIRECTORY . '/' . $directory)) {
				continue;
			}
			$branches[] = $directory;
		}
		$this->send_response($branches);
	}

	protected function send_response($data, $encode = true) {
		header( 'Content-Type: application/json; charset=utf8' );
		if ($encode) {
			echo json_encode($data);
		} else {
			echo $data;
		}
	}
}

$api = new API();
$api->stream_response(isset($_GET['branch']) ? $_GET['branch'] : 'branches');
