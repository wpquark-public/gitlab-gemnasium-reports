import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import {
	Icon,
	Divider,
	ExpansionPanelActions,
	Button,
} from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

/* eslint-disable jsx-a11y/accessible-emoji */
const styles = theme => ({
	root: {
		...theme.mixins.gutters(),
		paddingTop: theme.spacing.unit * 2,
		paddingBottom: theme.spacing.unit * 2,
		maxWidth: '1440px',
		margin: '0 auto',
	},
	loading: {
		minHeight: '400px',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	progress: {
		margin: theme.spacing.unit * 2,
	},
	column: {
		flexBasis: '33.33%',
	},
	columnHeading: {
		fontSize: theme.typography.pxToRem(15),
		flexBasis: '33.33%',
		flexShrink: 0,
	},
	columnSecondaryHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary,
		flexBasis: '50%',
	},
	iframe: {
		border: `1px solid ${theme.palette.divider}`,
		borderRadius: '4px',
	},
});

const getPriorityIconClass = priority => {
	switch (priority.toLowerCase()) {
		default:
			return 'help';
		case 'low':
			return 'notification_important';
		case 'medium':
			return 'warning';
		case 'high':
			return 'error';
	}
};

const countPriorities = (data, priority) =>
	data.reduce(
		(acc, cur) =>
			cur.priority.toLowerCase() === priority.toLowerCase()
				? acc + 1
				: acc,
		0
	);

const ReportDependency = props => {
	const { data, classes } = props;
	const highPriority = countPriorities(data, 'high');
	const mediumPriority = countPriorities(data, 'medium');
	const lowPriority = countPriorities(data, 'low');
	const unknownPriority = countPriorities(data, 'unknown');
	return (
		<ExpansionPanel>
			<ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
				<Typography className={classes.columnHeading}>
					DEPENDENCY REPORT ({data.length})
				</Typography>

				<Typography className={classes.columnSecondaryHeading}>
					{data.length === 0 ? (
						<span>No Errors... Yay!! 🎉</span>
					) : (
						<span>
							High: {highPriority} - Medium: {mediumPriority} -
							Low: {lowPriority} - Unknown: {unknownPriority}
						</span>
					)}
				</Typography>
			</ExpansionPanelSummary>
			<ExpansionPanelDetails>
				{data.length === 0 ? (
					<Typography>No errors were found.</Typography>
				) : (
					<List
						component="nav"
						style={{
							maxHeight: '500px',
							overflowY: 'auto',
							margin: '0 auto',
						}}
					>
						{data.map(item => (
							<Tooltip
								key={item.cve}
								title={item.file ? item.file : 'Unknown file'}
							>
								<ListItem
									component="a"
									href={item.url}
									button
									target="_blank"
								>
									<ListItemIcon>
										<Icon>
											{getPriorityIconClass(
												item.priority
											)}
										</Icon>
									</ListItemIcon>
									<ListItemText
										primary={item.message}
										secondary={`Priority: ${
											item.priority
										} Tool: ${item.tool}`}
									/>
								</ListItem>
							</Tooltip>
						))}
					</List>
				)}
			</ExpansionPanelDetails>
		</ExpansionPanel>
	);
};
ReportDependency.propTypes = {
	data: PropTypes.arrayOf(PropTypes.object).isRequired,
	classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const ReportCoverage = props => {
	const { url, label, classes } = props;
	return (
		<ExpansionPanel>
			<ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
				<Typography className={classes.columnHeading}>
					{label.toUpperCase()} COVERAGE REPORT
				</Typography>
			</ExpansionPanelSummary>
			<ExpansionPanelDetails>
				<iframe
					src={url}
					title={label}
					height={500}
					width="100%"
					frameBorder="0"
					className={classes.iframe}
				/>
			</ExpansionPanelDetails>
			<Divider />
			<ExpansionPanelActions>
				<Button
					size="small"
					color="primary"
					href={url}
					variant="flat"
					target="_blank"
				>
					OPEN IN NEW TAB
				</Button>
			</ExpansionPanelActions>
		</ExpansionPanel>
	);
};
ReportCoverage.propTypes = {
	url: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const Report = props => {
	const { loading, data, classes } = props;
	const { dependency, coverage } = data;

	return (
		<div className={classes.root}>
			{loading ? (
				<div className={classes.loading}>
					<CircularProgress
						className={classes.progress}
						color="secondary"
						thickness={1}
						size={100}
					/>
				</div>
			) : (
				<React.Fragment>
					<ReportDependency data={dependency} classes={classes} />
					{Object.keys(coverage).map(item => {
						if (coverage[item] !== '') {
							return (
								<ReportCoverage
									url={coverage[item]}
									label={item}
									classes={classes}
									key={item}
								/>
							);
						}
						return null;
					})}
				</React.Fragment>
			)}
		</div>
	);
};
Report.propTypes = {
	loading: PropTypes.bool.isRequired,
	data: PropTypes.arrayOf(PropTypes.any).isRequired,
	classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default withStyles(styles)(Report);
