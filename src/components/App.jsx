import React from 'react';
import PropTypes from 'prop-types';
import {
	MuiThemeProvider,
	createMuiTheme,
	withStyles,
} from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/teal';
import blueGrey from '@material-ui/core/colors/blueGrey';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import CircularProgress from '@material-ui/core/CircularProgress';

import Report from './Report';

const theme = createMuiTheme({
	palette: {
		primary: {
			light: teal[300],
			main: teal[500],
			dark: teal[700],
		},
		secondary: {
			light: blueGrey[300],
			main: blueGrey[500],
			dark: blueGrey[700],
		},
	},
});

const styles = style => ({
	root: {
		flexGrow: 1,
	},
	flex: {
		flex: 1,
	},
	menuButton: {
		marginLeft: -12,
		marginRight: 20,
	},
	container: {
		padding: 32,
	},
	loading: {
		display: 'flex',
		minHeight: 'calc(100vh - 64px)',
		alignItems: 'center',
		justifyContent: 'center',
	},
	progress: {
		margin: style.spacing.unit * 2,
	},
});

class App extends React.Component {
	static propTypes = {
		apiURL: PropTypes.string.isRequired,
		classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
		logoURL: PropTypes.string.isRequired,
	};

	constructor(props) {
		super(props);
		this.fetchBranches();
	}

	state = {
		initLoading: true,
		branches: [],
		branch: '',
		branchLoading: false,
		branchResult: {},
	};

	setBranches = branches => {
		this.setState(() => ({
			initLoading: false,
			branches,
			branch: '',
		}));
	};

	setActiveBranch = branch => {
		this.setState(
			() => ({ branch, branchLoading: true, branchResult: {} }),
			() => {
				const url = `${this.props.apiURL}?branch=${branch}`;
				fetch(url)
					.then(data => data.json())
					.then(data => {
						this.setState(() => ({
							branchLoading: false,
							branchResult: data,
						}));
					});
			}
		);
	};

	fetchBranches = () => {
		fetch(this.props.apiURL)
			.then(data => data.json())
			.then(data => {
				this.setBranches(data);
			});
	};

	refresh = e => {
		e.preventDefault();
		this.setState(
			() => ({ initLoading: true, branches: [] }),
			this.fetchBranches
		);
	};

	render() {
		const { initLoading } = this.state;
		const { classes, logoURL } = this.props;
		return (
			<MuiThemeProvider theme={theme}>
				{/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
				<CssBaseline />
				{/* App Bar */}
				<div className={classes.root}>
					<AppBar position="static">
						<Toolbar>
							<Typography
								variant="title"
								color="inherit"
								className={classes.flex}
							>
								<img
									alt="EForm"
									src={logoURL}
									style={{
										height: '40px',
										width: 'auto',
										marginRight: '12px',
										verticalAlign: 'middle',
										display: 'inlineBlock',
									}}
								/>
								EFORM DEVOPS REPORTS
							</Typography>
							<Button
								color="inherit"
								onClick={this.refresh}
								disabled={this.state.initLoading}
							>
								<Icon>refresh</Icon>
							</Button>
						</Toolbar>
					</AppBar>
				</div>
				<div className={classes.container}>
					{initLoading ? (
						<div className={classes.loading}>
							<CircularProgress
								className={classes.progress}
								color="primary"
								thickness={3}
								size={200}
							/>
						</div>
					) : (
						<React.Fragment>
							<Typography
								align="center"
								style={{ marginBottom: '24px' }}
							>
								{this.state.branches.length
									? this.state.branches.map(branch => (
											<Button
												color="secondary"
												variant={
													this.state.branch === branch
														? 'contained'
														: 'outlined'
												}
												key={branch}
												onClick={() =>
													this.setActiveBranch(branch)
												}
												style={{ margin: 10 }}
											>
												<Icon>list_alt</Icon>
												{branch.toUpperCase()}
											</Button>
									  ))
									: 'No Reports found at this moment'}
							</Typography>
							{this.state.branch !== '' && (
								<Report
									loading={this.state.branchLoading}
									data={this.state.branchResult}
								/>
							)}
						</React.Fragment>
					)}
				</div>
			</MuiThemeProvider>
		);
	}
}

export default withStyles(styles)(App);
