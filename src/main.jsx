import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

const apiURL = '/api.php';
const logoURL = '/eform.svg';

const AppMount = <App apiURL={apiURL} logoURL={logoURL} />;

document.addEventListener('DOMContentLoaded', () => {
	ReactDOM.render(AppMount, document.getElementById('app'));
});
